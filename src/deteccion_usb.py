import psutil
import shutil
import os
import time

while True:
        #Return all mounted disk partitions as a list. 
        #It might be device, mountpoint, fstype, etc. 
        particiones = psutil.disk_partitions(all=False)
        lista_particiones = []
        time.sleep(5)
        #This is the mednafen route. 
        archivos_mednafe =os.listdir('/home/pi/Juegos')
        for i in particiones:
                lista_particiones.append(list(i))
        #print(lista_particiones)


        #Identify is there is a new device 
        for i in lista_particiones:
                if i[0] == '/dev/sda1':
                        nombre_usb=i[1]

                        nombre = nombre_usb.split("/")
                        nombre_usb_detectada = nombre[3]
                        direccion_usb_detectada = '/media/pi/'+nombre[3]+'/' 

                        print('Memoria USB detectada', nombre_usb_detectada)
                        archivos_usb = os.listdir(direccion_usb_detectada)

                        #Compare the existing files with the new ones. If they don't exist, it copies them, otherwise it ignores them.
                        for i in archivos_usb:
                                #print("i:", i)
                                for j in archivos_mednafe:
                                        #print("j:", j)
                                        if i == j:
                                                pass
                                                #print("Duplicated File", i)
                                        elif i !=j and i.endswith('.nes'):
                                                shutil.copyfile(direccion_usb_detectada+i, '/home/pi/Juegos/'+i)
                        
                        #Next line stop the script when the copy finishes
                        quit()
                else:
                        pass
